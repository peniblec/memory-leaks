Necessary configurations for running cgit behind nginx on Debian.
* nginx
In =/etc/nginx/sites-available/git.domain.tld=:
#+begin_src
server {
	listen 80;
	listen [::]:80;

	server_name git.domain.tld;

	root /usr/share/cgit;
	try_files $uri @cgit;

	location @cgit {
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME /usr/lib/cgit/cgit.cgi;
		fastcgi_param PATH_INFO $uri;
		fastcgi_param QUERY_STRING $args;
		fastcgi_param HTTP_HOST $server_name;
		fastcgi_pass unix:/run/fcgiwrap.socket;
	}
}
#+end_src
* cgit
In =/etc/cgitrc=:
#+begin_src conf
css=/cgit.css
logo=/cgit.png
virtual-root=/

scan-path=/srv/git
#+end_src
* git
#+begin_src sh
$ sudo echo $(which git-shell) >> /etc/shells
$ sudo adduser git --disabled-password --shell $(which git-shell)
# Add SSH keys to ~git/.ssh/authorized_keys, prefixed with:
# "no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty "
$ sudo addgroup git-data
$ sudo adduser git git-data
$ sudo adduser www-data git-data
$ sudo mkdir /srv/git
$ sudo chown git:git-data /srv/git
$ sudo chmod g+s /srv/git
# For each repo:
$ sudo -u git git init --bare /srv/git/$repo
#+end_src
