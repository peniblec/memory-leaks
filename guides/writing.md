I worry a lot about how efficient my writing is.  I want to keep it as
straightforward (maximizing signal-to-noise ratio) and accurate
(citing sources and assumptions) as I can.

Since I keep catching myself making the same mistakes, and I do not
have automated tools to warn me about them yet, I figured I should
write down all the things I want to watch out for.

# Decruftification

## Sentences

- Watch out for verbs which hide the action (e.g. try, allow).
- Use simpler words as long as they are not more vague.

## Mails

- Pick your battles: people only have time for so many subjects.
  Focus on symptomatic relief; hint at systemic problems but wait
  until prompted before ranting.
- Tricks to make mails more digestible:
    - condense the point(s) into a tl;dr;
    - move details (investigation, alternatives, rationale) into
      footnotes or attachments;
    - spin new threads.

# Logic

When expressing causation, check that all causes have been explicitly
stated.  Be wary of assumptions^[I felt like adding "protect yourself
against misquotations", but those will probably happen anyway].

Conversely, if C can be deduced from A alone, consider rewriting
"since A and B, C" into "since A, C".  Unnecessary Bs can be
detrimental to the discussion and make it to go off-topic; beware:

- humorous overstatements that might be taken seriously;
- subconscious attempts at shoehorning a point that feels essential
  but is not actually relevant.

# Resources

- [IEEE's "Write Clearly and Concisely"](http://sites.ieee.org/pcs/communication-resources-for-engineers/style/write-clearly-and-concisely/)
- [Gitlab's Technical Writing Handbook](https://about.gitlab.com/handbook/product/technical-writing/)
