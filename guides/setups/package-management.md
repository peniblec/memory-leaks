Here are some things that I do often enough that I want to remember
how to do them properly, yet rarely enough that I will probably forget
this stuff if I do not write it down.

# Package managers

## APT

Add deb-src repositories to get:

- `changelog`
- `build-dep`
- `source`

# Installing stuff under `$HOME`

E.g. pandoc (compiled from source tarball with stack because there is
no 32-bit release) and ripgrep:

- programs:
    - install in `~/.local/bin`
    - add this folder to `$PATH` (in `.profile` and `.xsessionrc`)

- manpages:
    - install in `~/.local/share/man/man1`
    - in `~/.manpath`:

            MANPATH_MAP	~/.local/bin	~/.local/share/man

    - run `mandb --user-db`

- bash completion scripts:
    - install in `~/.local/share/bash-completion`
    - in `~/.bash_completion` (sourced by
      `/usr/share/bash-completion/bash_completion`):

        ``` bash
        for f in ~/.local/share/bash-completion/*
        do
            [ -f "$f" ] && . "$f"
        done
        ```
