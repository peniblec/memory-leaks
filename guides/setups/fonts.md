Fonts I find useful, plus messy configuration details.

# Symbola

Symbola seems to cover most characters I care about (although it does
not handle fancy emoji ligatures), and new versions are released
fairly soon after each revision of the Unicode standard.

Since distributions may not include the latest version, I often get it
directly from [the author's website](http://users.teilar.gr/~g1951d/).

# Installing fonts with Fontconfig

Gee, it sure is convenient to be able to install new fonts simply by
dropping them in `~/.fonts` and running `fc-cache`!

`/etc/fonts/fonts.conf`:

    	<!-- the following element will be removed in the future -->
    	<dir>~/.fonts</dir>

😨

Bunsenlabs symlinks `~/.fonts` to `~/.local/share/fonts`, but this
path is not mentioned anywhere in `/etc/fonts`.  What's the deal?

`/etc/fonts/fonts.conf`:

    	<dir prefix="xdg">fonts</dir>

fonts-conf(5):

> ## `<DIR PREFIX="DEFAULT">`
>
> This element contains a directory name which will be scanned for
> font files to include in the set of available fonts. If 'prefix' is
> set to "xdg", the value in the `XDG_DATA_HOME` environment variable
> will be added as the path prefix. please see XDG Base Directory
> Specification for more details.

[XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html):

> `$XDG_DATA_HOME` defines the base directory relative to which user
> specific data files should be stored. If `$XDG_DATA_HOME` is either
> not set or empty, a default equal to `$HOME`/.local/share should be
> used.

Seems legit then.

