Some day all of these will live in my dotfiles repository.

Until then…

# Bash

## `~/.bash_aliases`

``` bash
# Make Bash expand aliases before running sudo.
alias sudo='sudo '
```

## `.bashrc`

``` bash
# Unset the TTY's "stop" char, so that readline receives C-s.
if tty -s
then
    stty stop ''
fi
```

## `.profile`

``` bash
export EMAIL=…
```

# `.gitconfig`

``` ini
[core]
    quotePath = false
[user]
    name = …    # Not needed if GECOS field is set in /etc/passwd.
    email = …   # Not needed if $EMAIL is set.
[alias]
    root = rev-parse --show-toplevel
    forget = update-index --assume-unchanged
    remember = update-index --no-assume-unchanged
    wut = log --branches --decorate --graph --oneline
```
