---
title: "Peniblec's Memory Leaks"
subtitle: "still reachable: lots of words in many pages"
---

Hi!  I am a software engineer interested in [a bunch of things].

I kept worrying about forgetting stuff so I started taking notes; then
I started worrying about those notes being scattered over several
antediluvian computers so I figured I should put them all in one
repository and back them up somewhere.

This is the somewhere.

Also I'd like to join the [blogging bandwagon], and maybe publishing
this stuff is a good first step; in practice though I don't have much
to say.  `$DAYJOB` is as closed-source as it gets; I did send a few
trivial patches to some free-software projects, and I have an
ever-growing [list of itches] I intend to scratch someday, but they're
not exactly noteworthy.

I find it tough to cook up proper patches while desperately [trying to
keep up with my industry], [screwing around with my computers], and
leaving the keyboard long enough to convince my loved ones that I'm
still alive (and that I do love them).

This repository is a work in progress; I have a truckload of notes to
beat into shape before adding them here.  Some links above may seem
dead; actually, they were never alive to begin with.  I guess that
makes them… gestating?


[a bunch of things]: personal/interests.md
[blogging bandwagon]: reviews/blog-roll.md
[list of itches]: itches/
[trying to keep up with my industry]: reviews/
[screwing around with my computers]: guides/setups/
