#!/usr/bin/env python3

from argparse import ArgumentParser
from itertools import chain
import json
from os import path
from pathlib import Path
from subprocess import run
from tempfile import NamedTemporaryFile

from git import Repo

from helpers import deserialize_directories, generate_crumbs, PandocRunner


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        '--template', help='Pandoc template for HTML output.'
    )
    parser.add_argument(
        '--site-title', help='Prefix to add to <title>.'
    )
    parser.add_argument(
        '--lua-filter', dest='filters', action='append',
        help='Lua filter to run the page through.'
    )
    parser.add_argument(
        '--stylesheet', dest='css', action='append',
        help='CSS stylesheet to link to.'
    )
    parser.add_argument(
        'site_tree', help='JSON file describing the page tree.'
    )
    parser.add_argument(
        'target', help='Subfolder to generate an index for.'
    )
    parser.add_argument(
        'output', help='Path to the output file.'
    )
    return parser.parse_args()


def list_files(tree_file, folder):
    with open(tree_file) as tree:
        directories = deserialize_directories(json.load(tree))
    return directories[folder].subfolders, directories[folder].files


def has_title(document_path):
    pandoc = run(
        ('pandoc', '-t', 'json', document_path),
        check=True, text=True, capture_output=True
    )
    ast = json.loads(pandoc.stdout)
    return 'title' in ast['meta']


def list_pages(files):
    readme = None
    pages = []

    for f in files:
        page = Path(f).stem

        if page == 'README':
            readme = f
        else:
            pages.append(page)

    return pages, readme


def format_toc(directories, pages):
    dir_template = '<li><a href="{d}/index.html">{d}/</a></li>'
    page_template = '<li><a href="{p}.html">{p}</a></li>'

    dir_list = (
        dir_template.format(d=d) for d in directories
    )
    page_list = (
        page_template.format(p=p) for p in pages
    )

    return '\n'.join((
        '<ul>', *chain(dir_list, page_list), '</ul>'
    ))


def main(arguments):
    target = arguments.target
    folders, files = list_files(arguments.site_tree, target)
    pages, readme = list_pages(files)

    toc_title = f'Index for {target}' if target else 'Index'
    html_toc = format_toc(folders, pages)

    path_to_top = path.relpath('.', start=target)
    stylesheets = (path.join(path_to_top, s) for s in arguments.css)

    variables = {'crumbs': generate_crumbs(Path(target)/'index')}
    metadata = {}
    if arguments.site_title is not None:
        metadata['sitetitle'] = arguments.site_title

    pandoc = PandocRunner(
        arguments.output, arguments.template, arguments.filters,
        stylesheets, variables
    )

    if readme is None:
        with NamedTemporaryFile(suffix='.md') as dummy_readme, \
             NamedTemporaryFile(mode='w+') as toc:
            toc.write(html_toc)
            toc.flush()

            metadata['pagetitle'] = toc_title
            metadata['title'] = 'Index'

            pandoc.run(
                dummy_readme.name, include_after=(toc.name,), metadata=metadata
            )
            return

    repo_top = Repo(search_parent_directories=True).working_dir
    readme_path = Path(repo_top, target, readme)

    # If the README doesn't have a title, give a default to pandoc
    # out-of-band.
    if not has_title(readme_path):
        metadata['pagetitle'] = target or 'README'

    with NamedTemporaryFile(mode='w+') as toc:
        toc.write(f'<h1>{toc_title}</h1>\n')
        toc.write(html_toc)
        toc.flush()

        pandoc.run(
            readme_path, include_after=(toc.name,), metadata=metadata
        )


if __name__ == '__main__':
    main(parse_arguments())
