#!/usr/bin/env python3

from argparse import ArgumentParser
import json

from git import Repo

from helpers import compute_directories


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-o', '--output', help='Path to the output file.')
    parser.add_argument(
        'extensions', nargs='+',
        help='File extensions to consider when recording pages.'
    )
    return parser.parse_args()


def main(arguments):
    repo = Repo(search_parent_directories=True)

    directories = compute_directories(arguments.extensions, repo)
    serialized = ((d, directories[d].serialize()) for d in sorted(directories))

    with open(arguments.output, 'w') as out:
        json.dump(dict(serialized), out, sort_keys=True)


if __name__ == '__main__':
    main(parse_arguments())
