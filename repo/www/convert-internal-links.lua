local config = require 'config'

function Link(link)
  if link.target:match("^[%w]+://")
  then
    return link
  end

  for _, ext in pairs(config.EXTENSIONS)
  do
    link.target = link.target:gsub("%."..ext.."$", ".html")
  end

  return link
end
