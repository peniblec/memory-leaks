#!/usr/bin/env python3

from argparse import ArgumentParser
from os import path
from pathlib import Path

from git import Repo

from helpers import generate_crumbs, PandocRunner


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        '--template', help='Pandoc template for HTML output.'
    )
    parser.add_argument(
        '--site-title', help='Prefix to add to <title>.'
    )
    parser.add_argument(
        '--lua-filter', dest='filters', action='append',
        help='Lua filter to run the page through.'
    )
    parser.add_argument(
        '--stylesheet', dest='css', action='append',
        help='CSS stylesheet to link to.'
    )
    parser.add_argument(
        '--title', help='Page title.'
    )
    parser.add_argument(
        'page', help='Page to convert to HTML.'
    )
    parser.add_argument(
        'output', help='Path to the output file.'
    )
    return parser.parse_args()


def main(arguments):
    repo_top = Repo(search_parent_directories=True).working_dir
    path_to_top = path.relpath(repo_top, path.dirname(arguments.page))
    stylesheets = (path.join(path_to_top, s) for s in arguments.css)

    page_path = Path(arguments.page).resolve().relative_to(repo_top)

    pandoc = PandocRunner(
        arguments.output,
        arguments.template,
        arguments.filters,
        stylesheets,
        variables={'crumbs': generate_crumbs(page_path)},
    )

    pandoc.run(
        arguments.page,
        metadata={'pagetitle': arguments.title,
                  'sitetitle': arguments.site_title}
    )


if __name__ == '__main__':
    main(parse_arguments())
