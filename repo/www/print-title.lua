title = ''

titlefilter = {
  Str = function (element)
    title = title .. element.text
  end,

  Code = function (element)
    title = title .. element.text
  end,

  Space = function (element)
    title = title .. ' '
  end,
}

function Pandoc(doc)
  pandoc.List.map(
    doc.meta.title,
    function (inline)
      pandoc.walk_inline(pandoc.Span(inline), titlefilter)
    end
  )
  print(title)
  os.exit(0)
end
