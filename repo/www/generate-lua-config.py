#!/usr/bin/env python3

from sys import argv


TEMPLATE = '''\
local config = {{}}

config.EXTENSIONS = {{ {EXTENSIONS} }}

return config
'''


def _quote(s):
    return f"'{s}'"


def main(arguments):
    pairs = (arg.split('=') for arg in arguments)

    formatters = {
        'EXTENSIONS': lambda v: ', '.join(map(_quote, v.split()))
    }

    parameters = {
        key: formatters[key](value) for (key, value) in pairs
    }

    print(TEMPLATE.format_map(parameters), end='')


if __name__ == '__main__':
    main(argv[1:])
