#!/bin/bash

list-tracked ()
{
    GIT_LITERAL_PATHSPECS='' git ls-files '*.md' '*.org'
}

count-leaks ()
{
    let pages=0
    let words=0

    while read filename
    do
        let words+=$(git show HEAD:${filename} | wc -w | cut -d' ' -f1)
        let pages+=1
    done

    echo ${words} ${pages}
}



read words pages < <(list-tracked | count-leaks)

pattern="[0-9]* words in [0-9]* pages"
actual="${words} words in ${pages} pages"

sed -i "s/${pattern}/${actual}/" README.md
