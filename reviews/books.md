# Silent Spring

> **Under primitive agricultural conditions the farmer had few insect
> problems.  These arose with the intensification of agriculture — the
> devotion of immense acreages to a single crop.  Such a system set
> the stage for explosive increases in specific insect populations.**
> Single-crop farming does not take advantage of the principles by
> which nature works; it is agriculture as an engineer might conceive
> it to be.  Nature has introduced great variety into the landscape,
> but man has displayed a passion for simplifying it.  Thus he undoes
> the built-in checks and balances by which nature holds the species
> within bounds.  **One important natural check is a limit on the
> amount of suitable habitat for each species.  Obviously then, an
> insect that lives on wheat can build up its population to much
> higher levels on a farm devoted to wheat than on one in which wheat
> is intermingled with other crops to which the insect is not
> adapted.**

Emphasized portions are the sentences that conveyed the message
efficiently to my taste: measurable facts punctuated with concise
claims.

The middle sentences probably appeal to people who already buy the
author's point: they re-state their own convictions with added
cosmological sugar ("the principles by which nature works"),
antagonizing metaphors ("agriculture as an engineer might conceive
it"), sweeping generalities ("man has displayed a passion for
simplifying it"), and romantic allegories^[See also: the Gaia
hypothesis and its [criticism][gaia-critic].] ("the built-in checks
and balances by which nature holds the species within bounds").

These sentences take a grim and uncomfortable (and well-documented)
reality, and transform it into a lyrical fresque of Good versus Evil.
They take what could be the basis for a technically deep and
insightful report, and twist it into a sublime, emotional, and
*simplified* depiction of some Universal Truth.

What bothers we with this vocabulary is that it is indistinguishable
from zealotry.  Cults thrive on broad, universal explanations for
complex issues: Us versus Them, individual redemption for collective
sins, Mother Nature…  To me, these tropes are red flags; they invite
me to wonder where the fallacy is: why should the speaker need to
appeal to some nebulous higher principle?  Is it as universal as they
claim?  Why can't they root their point into observable evidence?

I actually do buy the authors's point; I merely wish it didn't come
with fake gold plating, since it makes people I want to share it with
go "Wait, what's up with that cheap cheesy decoration?  It looks
silly".

[gaia-critic]: https://en.wikipedia.org/wiki/Gaia_hypothesis#Criticism_in_the_21st_century

> Parathion is one of the most widely used of the organic phosphates.
> It is also one of the most powerful and dangerous.  Honeybees become
> ‘wildly agitated and bellicose’ on contact with it, perform frantic
> cleaning movements, and are near death within half an hour.  A
> chemist, thinking to learn by the most direct possible means the
> dose acutely toxic to human beings, swallowed a minute amount,
> equivalent to about .00424 ounce.  Paralysis followed so
> instantaneously that he could not reach the antidotes he had
> prepared at hand, and so he died.  Parathion is now said to be a
> favorite instrument of suicide in Finland.  In recent years the
> State of California has reported an average of more than 200 cases
> of accidental parathion poisoning annually.  In many parts of the
> world the fatality rate from parathion is startling: 100 fatal cases
> in India and 67 in Syria in 1958, and an average of 336 deaths per
> year in Japan.  Yet some 7,000,000 pounds of parathion are now
> applied to fields and orchards of the United States—by hand
> sprayers, motorized blowers and dusters, and by airplane.  The
> amount used on California farms alone could, according to one
> medical authority, ‘provide a lethal dose for 5 to 10 times the
> whole world’s population.’

It gets worse.

> Potentiation seems to take place when one compound destroys the
> liver enzyme responsible for detoxifying the other.

Chapter 4 in a nutshell:

> In 1943, the Rocky Mountain Arsenal of the Army Chemical Corps,
> located near Denver, began to manufacture war materials.  Eight
> years later the facilities of the arsenal were leased to a private
> oil company for the production of insecticides.  Even before the
> change of operations, however, mysterious reports had begun to come
> in.  Farmers several miles from the plant began to report
> unexplained sickness among livestock; they complained of extensive
> crop damage.  Foliage turned yellow, plants failed to mature, and
> many crops were killed outright.  There were reports of human
> illness, thought by some to be related.
>
> The irrigation waters on these farms were derived from shallow
> wells.  When the well waters were examined (in a study in 1959, in
> which several state and federal agencies participated) they were
> found to contain an assortment of chemicals.  Chlorides, chlorates,
> salts of phosphoric acid, fluorides, and arsenic had been discharged
> from the Rocky Mountain Arsenal into holding ponds during the years
> of its operation.  Apparently the groundwater between the arsenal
> and the farms had become contaminated and it had taken 7 to 8 years
> for the wastes to travel underground a distance of about 3 miles
> from the holding ponds to the nearest farm.  This seepage had
> continued to spread and had further contaminated an area of unknown
> extent.  The investigators knew of no way to contain the
> contamination or halt its advance.
