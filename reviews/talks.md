# Sandi Metz - Nothing is Something

:::: tags
- OOP
- Ruby
::::

Some insightful thoughts on OOP.

Clever examples of message-passing to get rid of conditionals.

Prefer composition with dependency injection to inheritance:
inheriting to override a method really means that the parent class had
a default "null" behaviour that can be injected explicitly.

Name the role, make an interface for it, and make classes that
implement each behaviour (the null one and the specialized one).  This
will make it easier to compose specializations (i.e. save you from
multiple inheritance).


# Sandi Metz - Rules

:::: tags
- OOP
- Ruby
::::

Some theory-crafting on rule-following, then the actual rules, which
amount to

- small classes,
- tiny methods,
- few parameters.

At the 16-minute mark, an interesting interaction between Metz and a
programmer starting to apply these rules: the programmer wonders when
they will be able to understand the new application "the same way"
they understood the old one.  I.e. will they ever reach a point where
they have a complete mental map of every interaction between every
object, the same way they used to have a complete mental map of the
old monolithic, sequential application?

Metz's response is that nope, this will never happen; they will "cease
to care" instead.  In exchange, they will be able to make localized
changes without worrying about breaking the whole application.


# Fred George - The Secret Assumption of Agile

:::: tags
- Agile
- OOP
- programming methods
- project management
- training
::::

Advice on when to refactor:

- *after* design, *before* writing unit tests for the new stuff:
  prepare ground for the addition to fit right in;

- *after* implementing the new behaviour, *before* integrating.

Goes over the usual code smells taught during the training he gives
(conditionals, getters & setters, class names ala "Manager", too many
instance variables).

Mentions a requirement for training **retention**: skills must be
applied within a month after receiving the training, otherwise the
rationale will be lost.

Questions:

- Does he know of open-source projects that showcase this style of
  programming?
    - Smalltalk, some NodeJS libraries.

- Does he rely on naming conventions?
    - Quite a lot.


# Josh Triplett - Intel and Rust - The Future of Systems Programming

:::: tags
- C
- Rust
- systems programming
::::

## Systems programming and C

Systems programming
:   > Anything that isn't an app

    Platforms for other programs to run on top of: kernels, VMs, even
    web browsers.

Why did it take so long to move away from assembly for BIOS, firmware
and bootloaders, despite C being readily available?

- C had to have enough *compelling features* over assembly: "some
  degree of type safety", more readable code…
- C had to provide **feature parity**: developers had to feel
  confident that they wouldn't be missing features from assembly.

You could mostly do anything you did in C that you did in assemlby,
and you had an "escape hatch" (the `asm` directive).

The problems with C go beyond "buffer overflows".

> Memory safety is the bare minimum requirement these days.

**Usability and productivity go hand in hand with security:**

> The less code you need to write to accomplish something, the less
> chance you have of introducing bugs.

One doesn't have to reach very far to find languages with more
*compelling features* than what C offers.  What does "parity with C"
entail, though?

## Rust

Rust started out as a safe and performant language for complex
applications, such as web browsers.

Key feature #1: automatic memory management **without** a garbage
collector.

(Adding Rust to a codebase is jokingly refered to as "Oxidation")

Key feature #2: safe concurrent programming.

### Parity with C

Rust can handle C functions, structures, unions and pointers.

Soon in stable Rust™: varargs, smaller binaries, robust inline
assembly (there already exist builtins for SIMD instructions),
bfloat16, more structure layout shenanigans.


# Bryan Cantrill - Oral Tradition in Software Engineering

:::: tags
- C
- History
- UNIX
::::

"Oral tradition" as in "educational stories we pass down for
generations", not necessarily actually oral.

- USENET's "[Story of Mel]" is one of our oldest enduring "tales".

- Cantrill attributes the scarcity of 80s and 90s lore to the "deeply
  proprietary" culture of the era.
    - Though he does manage to squeeze in an emotional reading of a
      Hemingway parody called "[The Bug Count Also Rises]".

        > Many days he stood for a long time and watched the rain and
        > the shuttles and drank his double-tall mochas.  With the
        > mochas he was strong.

        > […]

        > They sat silently for awhile, then he asked Michaels, “I
        > need you to triage for me.”

        > Michaels looked down. “I don't do that anymore,” he said.

- The 00s introduced a new era, where re-telling a story became as
  simple as sharing the link to a video.

- Even without exploring the Internet, our own source bases are
  "pulsing with narrative".

- Ends with a historical survey of UNIX lore buried in the codebase.
    - To Roger Faulkner suggesting asking Dennis Ritchie to settle the
      debate on why C lacks a logical XOR:

        > … You can *ask* The Lord?

      (Spoiler: to Cantrill's dismay, it was intentional, because such
      an operator could not short-circuit, and "`(A!=0) ^ (B!=0)`
      isn't that much worse to write", though I personally find it
      less *legible* than `A ^^ B`.)

      The point to the otherwise entertaining story being: *people are
      going to care about your intent*.  Younger generations yearn to
      understand what's going on and how they can perpetuate their
      forebears's legacy.

      (… They also need enough context to evaluate how valuable this
      legacy truly is.)

        > I really cared about what Dennis thought! (… Before I knew
        > he was wrong.)

[Story of Mel]: https://en.wikipedia.org/wiki/The_Story_of_Mel
[The Bug Count Also Rises]: http://www.workpump.com/bugcount/bugcount.html
