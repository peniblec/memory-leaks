# linux.conf.au 2018

## Making Technology More Inclusive Through Papercraft and Sound

By Andrew Huang.

I like how the talk goes over a range of cross-domain topics:

- high-level motivations:

  Improving inclusiveness is necessary to make open-source actually
  empower people; right now a very small subset of the population is
  computer-savvy enough to take advantage of it.  If the situation
  does not improve, a handful of developers will hold a lot of power
  over lots of alienated users, and lawmakers may resort to
  "preposterous" solutions to attempt to regain control, e.g. license
  bonds for software developments.

- Kickstarter campaign management
- design choices & rationale
    - "China-ready"
    - "patience of a child" constraint
- gory hardware details
- the end result

## QUIC: Replacing TCP for the Web

By Jana Iyengar.

Starts by introducing impressive application performance improvements,
although where were those measured?  E.g. rural areas?

Advantage that can already be inferred from the layer view: QUIC needs
fewer handshakes than TCP+TLS.

Achieves 0-RTT when the server's cryptographic credentials are known.

Supports "stream multiplexing": the upper layer (e.g. HTTP) can
transfer multiple objects independently in a single connection.
Losing part of one object does not block the others: retransmission is
managed at the stream level, not at the connection level.

On top of UDP: allows userspace (Chrome) implementation.

> If you think of layers as a set of functions, things that you want
> done, UDP is not a transport protocol.

I.e. UDP does not provide reliability, same-order delivery…

Jana was "in the SCTP bandwagon".

They actually have *better performance improvements* for *bigger
latencies*?  Nice.

> § QUIC improvements by country

👏

(Of course the end goal is probably to make sure regions with poor
connections do not miss out on the adfest; still, these remain welcome
technical improvements)

Transport headers are encrypted to prevent "middlebox ossification".
They left a *single* byte unencrypted (the flags byte): this allowed
middleboxes to observe that it kinda had the same value on most
connections, assume that this was a "nominal" value, and block traffic
when this value differed.

## You Can't Unit Test C, Right?

By Benno Rice.

- Mentions [Check](https://libcheck.github.io/check/) and
  [Kyua](https://github.com/jmmv/kyua).
- Factor your boilerplate into libraries, especially the ugly hacks.
- Keep `main` small so that you don't need to test it so much.

## Changing the world through (fan-)fiction

By Paul Fenwick.

Reading fiction is a convenient way to get us to think through
concepts we had not considered before.  By re-purposing a familiar
setting, *fan*fiction lowers the barrier to entry to the writing
exercise: it makes it easier for the writer to get their point across
and to reach their audience.

Some recommendations:

- The Last Ringbearer
- [My Little Pony: Friendship is Optimal]
- [Harry Potter and the Methods of Rationality]

Our media teaches us what is normal.  Hence fiction opens up ways to
improve the status quo by acquainting us to new ideas.

Another recommendation: Steven Universe.

Mainstream and folklore stories feature a fair amount of unhealthy
relationships; this is problematic because repeated exposure helps
normalization^[I find that SMBC is a positive example of this effect:
it regularly (and, AFAICT, fairly randomly) features gay couples in
comics where the joke is *not* about homosexuality].

In Japan, doujinshi is considered normal and "adding value to the
brand", whereas similar things are flagged as "copyright infringement"
in other countries.

[My Little Pony: Friendship is Optimal]: https://www.fimfiction.net/story/62074/Friendship-is-Optimal
[Harry Potter and the Methods of Rationality]: http://www.hpmor.com/

## Lessons from three years of volunteering to teach students code

By David Tulloh.

Takeways:

1. Volunteering in schools is easy and fun.
2. We should care about what is taught in schools.
3. We should get involved and support schools teaching IT.

CSIRO: Australian program to get professional developers to teach in
schools.

[Pixees](https://pixees.fr/) seems to be a French equivalent.

Tried to move students from "programmers" to "developers" by evoking:

- automated testing
- version control
- bug tracking
- code review

An audience member noted that while programs ala CSIRO are helpful,
this should be organized at the government policy level.

