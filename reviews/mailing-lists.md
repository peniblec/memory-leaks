# Emacs

## 2019-01

### [Apropos 54f297904e0c: Temporarily comment out CC Mode from tests which are incompatible with it.][cc-mode/electric-pair]

A thread that feels very representative of the friction between
"keeping things working as they always did" vs. "simplifying that huge
lumbering beast of a codebase".  I find [Stefan Monnier's
intervention][cc-mode/electric-pair-stefan] helpful because it
captures both sides of the argument quite well:

> >> I understand that there's a transition needed between these two and this
> >> intermediate state can require more work, but it's important to keep the
> >> long term goal in mind when designing the current solution.
>
> > Whose long term goal?
>
> At the very least mine.
>
> > My goal, both long and short term, is to keep CC Mode
> > working properly.
>
> That's orthogonal.
>
> To give you a similarly general goal, my own goal is to make it so that
> **any feature which makes sense in many major mode be implemented once and
> then used in the relevant major modes, rather than being implemented
> independently for those major modes.**
>
> This is both for uniformity between major modes, and because it both
> simplifies and improves many major modes (which would otherwise either
> not provide the feature or only in very primitive ways).
>
> **And those maintainers like you who did go to the trouble of being early
> implementers of the feature suffer through the pain of having to adapt
> their code once the generic version of the feature becomes available.**
> Sometimes even at the cost of having the new feature working slightly
> worse in some corner cases.
>
> But many of them are quite happy to be able to drop their old code and
> get rid of that responsibility (i.e. bug reports about regressions can
> be redirected to Emacs maintainers).

On a non-technical note, this thread allowed RMS to advertise the [GNU
Kind Communications Guidelines][gnu-kind-communication]:

> To ask "who started it" is to oversimplify.  Often what happens
> is that a little harshness creeps into a discussion, then people
> react to that in a way that is a little more harsh.  So nobody
> "started it" but multiple people exacerbated it.
>
> **Thus, part of the effort is, when we feel harshness coming at us,
> to damp it down rather than hitting back.**

[cc-mode/electric-pair]: https://lists.gnu.org/archive/html/emacs-devel/2019-01/msg00360.html
[cc-mode/electric-pair-stefan]: https://lists.gnu.org/archive/html/emacs-devel/2019-01/msg00518.html
[gnu-kind-communication]: https://www.gnu.org/philosophy/kind-communication.html

### [Documentation about the completion framework][completion-documentation]

Helpful pointers for whoever wants to dig into completion plumbing.
Personally, I struggled to get to grips with it when [debugging some
`icomplete` issue][icomplete-issue], relying on the source for
information, and giving up while trying to write an automated test
case.

[completion-documentation]: https://lists.gnu.org/archive/html/emacs-devel/2019-01/msg00504.html
[icomplete-issue]: https://gitlab.com/peniblec/memory-leaks/commit/dcc0c05dc1f6a22645bf9355b72aa44b49776620

## 2019-05

### [\[RFE\] Migration to gitlab][migration-gitlab]

Eli Zaretskii's comprehensive answer to the question: "How could
moving to a GitLab-centric workflow possibly make a maintainer's life
worse?"

My interpretation:

Emacs's development infrastructure is pretty threadbare by modern
standards.  Contributions, drive-by or otherwise, linger as
unremarkable Debbugs attachments until someone takes the time to try
the patches out.  Once the discussion branches into multiple
sub-threads, it can be hard to track the "current state" of the patch
series.

A Continuous Integration server [exists][emacs-ci], but it only tracks
branches on Savannah AFAIK; regressions can only be identified if
someone takes the time to run tests locally (at best) or once the
patches have been committed to the Emacs repository (at worst).
Contrast with GitLab-centric projects where anyone "driving-by" is
automatically greeted with a test suite report.

We are reliant on the unyielding dedication of extremely competent
maintainers to review contributions, condemned to spend CPU cycles
checking for breakage and waste heartbeats pointing out trivial
formatting issues, before they can move on to higher-level review
(e.g. feature design, documentation clarity).  The push for "better
tooling" is motivated by the prospect of lowering the bar for
participating in maintenance.

Yet despite the dismal state of the current server-side tooling,
GitLab *as a maintainer frontend* lacks many features that some Emacs
veterans expect.  Some of the facilities Eli lists may eventually get
equivalents in GitLab's web UI, but neither the GitLab UI nor the
browser it runs on will ever provide a completely reprogrammable
environment one can enhance on-the-fly.

To borrow terms from [Josh Triplett's comparison of Rust vs. C for
systems programming][compelling-vs-parity], the GitLab server offers
*compelling features* over Debbugs, but the GitLab web UI does not
provide *feature parity* with Emacs.

AFAICT, some paths of least resistance for better development tooling
would be

- extending Debbugs to push a patch series as a branch on EMBA, and
  report the test results,
- adding scripts to check commit messages against the expected
  Changelog format, and suggesting templates based on the diffs
  (i.e. what `diff-add-change-log-entries-other-window` does).

Full interoperability between maintainers who would like a web UI for
reviews, and maintainers who prefer the versatility of simply quoting
patches in a mail-composition buffer sounds tricky.  The CI server
would need to break quoted-patch emails down into { quoted hunk,
comment } pairs so that the comments show up correctly (i.e. properly
cross-referenced to the hunk being discussed) on the web UI; not sure
if some platform already supports that; maybe [sourcehut][srht] took a
stab at it?

[migration-gitlab]: https://lists.gnu.org/archive/html/emacs-devel/2019-05/msg00306.html
[emacs-ci]: https://emba.gnu.org/emacs/emacs/pipelines
[compelling-vs-parity]: https://hub.packtpub.com/rust-is-the-future-of-systems-programming-c-is-the-new-assembly-intel-principal-engineer-josh-triplett/
[srht]: https://sourcehut.org/
