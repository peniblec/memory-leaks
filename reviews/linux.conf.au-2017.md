# linux.conf.au 2017

## General comments

Re-stating the audience's questions before replying is helpful.

## Choose Your Own Adventure, Please!

Keynote by Pia Waugh.

Warns against short-sighted itch-scratching; wants to encourage more
long-lasting systemic change.  To contrast with Maciej Cegłowski, who
warns against [ivory-tower wank] in Sillicon Valley, where no-one
seems interested in working on the severe poverty problems nearby.

(To be fair, Pia does say we need both "symptomatic relief" and
systemic change.)

41:30

> My favourite story from my studies with martial arts was actually
> about two monks walking around.  They're walking along, elder one,
> younger one, and when they get to the river, a person comes and says
> "I'm being chased by robbers, can you help me across the river
> please?".  The older monk says "Yep, not a problem", picks them up
> and carries them across (because they're hurt).  The person gets
> away.  And they're walking along, still in silence, and the younger
> monk says: "… You know, back at the river back there"; the older
> monk says "Yeah?"; the younger monk says "I thought we had taken a
> vow of silence".  The other goes, "Yeah?".  "… Should you have
> spoken to that person?", and the older monk says: "I put that person
> down back at the river.  Why haven't you?"

That story appeals to me: it's got some sort of
Jesus-ish-unconditional-forgiveness-Zen vibe that feels reassuring,
"it's OK to make mistakes, as long as you aimed for the Greater Good,
focus on the Spirit of the Law instead of upholding the Letter".  But
slippery slope turns that into "move fast and break things",
consequences and accountability be damned.

You can even link that to ["fussy" compilers] and false alarms: why
should Buddhist GCC warn on Vow-of-Silence violation if it's not
actually a problem?  The warning should be refined, the diagnosis
should be smarter, the standard amended, otherwise how do you
distinguish between the shades of red?

["fussy" compilers]: https://lists.gnu.org/archive/html/emacs-devel/2016-04/msg00190.html
[ivory-tower wank]: http://idlewords.com/talks/superintelligence.htm

## Stephen King's practical advice for tech writers

By Rikki Endsley.

Lots of pointers, e.g. [The Care and Feeding of the
Press](http://netpress.org/care-feeding-press/).

Suggested outline:

- intro (invite the reader in)
- state the problem (background)
- solution
- (for tech article, tutorial, whitepaper: technical stuff (howto, FAQ))
- conclude (important dates, action items)

Parasite words: "very", "some".  Be mindful of slang.

## Sharing the love: making games with PICO-8

By John Dalton.

> Sad old people, longing for the glory days

PICO-8 restores the "Democracy of Creating".

Kids get the point of sharing without having to be "encouraged" by
licences.

## Writing less, saying more: UX lessons from the small screen

By Claire Mahoney.

- "mobile" is not necessarily "on the move"
- a "mobile" app does not have to be a "diet" version of the original

Users do not expect the functionality to be diminished.

> Context can be better than words

(I feel like there is a connection to be made here with namespaces in
programming languages.)

Patterns are good, repetition is not.

Defining purpose with "when X, I want Y so I can Z" helps "keeping it
real" and reminding you of the user out there.

## Mamas Don't Let Your Babies Grow Up to Be Rock Star Developers

By Rikki Endsley.

When writing job descriptions, stop asking for rock stars.  Focus on:

- job requirements
- job environment

Makes it easier for people to figure whether they will fit in.

Look for developers interested in making *others* succeed, learning
*new* skills; make sure they are accessible, they use the best tool
for the job, and they are able to innovate, lead, and collaborate with
a diverse mix of people.

If you have a rockstar on your hands, make sure the janitors still get
some credits.

## Why haven't you licensed your project?

By Richard Fontana.

"Post open-source" has actually been a thing for a while: the term
describes the widespread trend of not attributing a license to one's
project.

Berne convention says that copyright is automatic, so this POSS
software might be implicitly "proprietary".  Why worry?  There is a
lot of proprietary software already.

Not putting on a license constitutes a statement for some developers.

Some attempts at public-domain dedication:

- [WTFPL](http://www.wtfpl.net/)
- [Unlicense](http://unlicense.org/)
- [0-clause BSD](http://landley.net/toybox/license.html)
- [BOLA](https://blitiri.com.ar/p/bola/)
- [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/)

## Handle Conflict, Like a Boss!

By Deb Nicholson.

Conflict mostly comes from missing information, mismatched goals.

Avoidance, accomodation and assertion each have their own issues as
conflict-handling strategies.

Using historical motivations can help give credit to new ideas.

Hypotheticals such as "What's the worst that could happen?" help
identify the root issues people will not directly talk about.

No ad hominem. No name-calling. Period.  Beware of [Contempt Culture].

Setting expectations can help enforce a civil tone and constructive
criticism.

[Contempt Culture]: https://blog.aurynn.com/2015/12/16-contempt-culture

## The journey of a word: how text ends up on a page

By Simon Cozens.

Very interesting explanations on the lengths Unicode must go to in
order to turn humanity's sprawling mess of written communication
methods into rigorous rules that a computer can understand.

Some diacritics can be encoded either with a single code point or a
vowel plus a combining code point; this is because Unicode intends to
have one code point for *every character that other encodings have
ever contained*.

Cozens is publishing a free online book on the subject: [Fonts and
Layout for Global Scripts].

[Fonts and Layout for Global Scripts]: https://simoncozens.github.io/fonts-and-layout/

## Surviving the Next 30 Years of Free Software

By Karen Sandler.

Is copyright assignment to big organizations (Canonical, FSF?) the
solution to problems we cannot anticipate?

Wills are tricky: recipients might be taxed on the "monetary value" of
the "legacy".

Using a trust as a "legal hack": would build a "registry" of free
software; the trust can map handles to contact information to preserve
anonymity.

The idea is vaporware for now, since this trust cannot be built
without debating a lot of finer points.

> The best gift you can give to the people you love is to make sure
> they're prepared for when you're gone.

## The relationship between openness and democracy

By Pia Waugh.

Openness creates a natural incentive for "doing the right thing".

Some people think shady deals which allow politicians to make huge
amounts of money from the industry are fair game, since they have to
get the investments they made during their campaign back.

On "policy-based evidence" as an alternative to evidence-based policy:

> That's rather funny'n'clever'n'witty… Oh shit, you're serious.

How representative and legitimate are elected individuals?  Never mind
the participation rate, most people vote for (or against) one or two
things, not the whole program.

> (13:00) Everyone loves to kick public servants; **everyone**.

> (14:30) I was gonna start a cartoon.  And the first thing was gonna
> be someone saying "I'm surprised that you're working in government,
> I would've thought you'd disagree with X, Y, Z."  OK.
>
> The second panel someone saying to me "I just can't believe you're
> working in government!  I thought you had *integrity*!  I thought
> you would disagree with all of these things!"  … *OK*.
>
> The third person says "YOU MOTHER-"…  Anyway, goes on a complete
> tirade, I'll probably get hit on the head.
>
> The fourth panel is me running off in the distance.  Into the
> sunset.  And the three people saying to each other "Why are there no
> good people in government?"

"Consulting the public" used to be a point on a checklist, not
intended to yield useful outputs.

## JavaScript is Awe-ful

By Katie McLaughlin.

In JavaScript, functions have to add `var` explicitly to their local
variable declarations, otherwise they will assign to global variables.

``` javascript
> [] + []
""
> [] + {}
[object Object]
> {} + []
0
> {} + {}
NaN
```

JavaScript is a registered trademark; ECMAScript is the actual,
*standardised*, **versioned** language.

Some examples of things which can be accomplished without JavaScript:
<http://youmightnotneedjs.com/>.

Cross-compilers alleviate some of the pain; one has to be careful with
their prefered language's warts though.

In Ruby, `&&` and `and` do not have the same precedence with respect
to `not`.

## Data Structures and Algorithms in the 21st Century

By Jacinta Catherine Richardson.

Voronoi diagrams have a lot of applications:

- modeling the capacity of wireless networks
- robot navigation
- mouse hoverstate

Fourier transforms help with data compression.  Naively: O(n²); from
the sixties onward: O(n log(n)).  Nearly Optimal Sparse Fourier
Transform (2012): O(k log(n)), helps on-the-fly data compression.

Singular Value Decomposition helps with pattern recognition/comparison
by allowing to express e.g. rotations.

> New stuff!

Evolutionary algorithms (a form of AI/machine learning) to find
optima:

- a function to tell "is this good enough?"

Genetic algorithms (a form of evolutionary):

- fitness criteria
- swap information ("breed")
- random-ish variations

> Setting up the fitness criteria and the initial conditions for
> genetic algorithms […] is as much art as it is science.

Artificial Immune Systems (90s) is used in computer security.

Swarm algorithms: agents share the value of their findings and
converge.  Used e.g. to locate cancer; considered for e.g. traveling
sales person problem, unmanned cars.

Bacterial Foraging Optimization; Shuffled Frog Leaping;
Teaching-Learning-Based Optimisations.

[Foldit](http://fold.it) is an experiment consisting in making humans
solve hard problems (e.g. protein folding) through competitive gaming.

Graph isomorphism is *hard*.  Easy to verify, hard to solve. Until a
week ago: we can now solve them in quasi-polynomial time.

## My personal fight against the modern laptop

By Hamish Coleman.

Ports, durability, keys are getting worse.

Plugging an older keyboard on newer Thinkpads presents issues:

- the motherboard sends in high-voltage current to enable backlight
- some keys don't work; the firmware must be changed (and then
  re-encrypted)

Sharing firmware patches is challenging; most end-users have no idea
what these even are; some of them run Windows and cannot easily use
the patching tools.

Newer firmwares seem to be signed; this will probably make them harder
to tweak.
