Support for color fonts [[https://lists.gnu.org/archive/html/emacs-devel/2019-04/msg00996.html][has landed]] on version 27.0 (master).
Remaining issues:

* DONE Fix font-hinting =--with-cairo=

Bug#35781 fixed by Yamamoto Mitsuharu.

* TODO Check whether combining sequences work

For example: regional flags, ZWJ sequences from

- ZWJ sequences from
  <https://unicode.org/emoji/charts/emoji-zwj-sequences.html>:
  - 🧟‍♂️, 🧟‍♀️
  - 🐕‍🦺
- regional indicators: 🇪🇺, 🇫🇷

* TODO Add a convenient way to set a font for all emojis

AFAICT there is no convenient way to =set-fontset-font= e.g. Noto
  Color Emoji for "all emojis"; valid TARGETs for this function are
    - single characters
    - character ranges
    - scripts, as defined in =script-representative-chars=
    - charsets
    - nil = any character not already covered by another font-spec
