# Emacs

- center/top point when
    - moving to text/tag search hit
    - going to man page section
    - opening eww on an anchor
    - visiting a function from its Help buffer
- scroll-lock-mode
    - cursor stuck on lines wrapped by visual-line-mode
    - cursor stuck on lines taller than regular height (see maybe
      bug#36421?)
    - scroll when going down newline with forward-…
- matching-paren analysis fails: `;; (here is\n;; a comment)`
- some modes redefine C-M-h, which makes this binding hard to
  override; could mark-defun consult functions/values exposed by major
  modes instead?
- Man-mode: make isearch skip end-of-line hyphens
- when opening `.gpg` files in a TTY, some characters (e.g. TAB) are
  swallowed by Emacs instead of being forwarded to the gpg prompt;
  these characters are then inserted in the decrypted file's buffer
  (see `epa-file-insert-file-contents`)
- let TRAMP fetch Google Drive credentials from .authinfo.gpg
- calendar uses default X resources when run in its own frame; see
  `calendar-frame-parameters`, `x-handle-named-frame-geometry`,
  `(make-frame '((name . "")))` vs `(make-frame '((title . "")))`
- use compilation-mode-line-… faces for compilation-num-… indicators
- "é" is not erased with a single DEL stroke, despite [Unicode
  conventions]
- sort tar archive members by name; serving suggestion:

    ``` elisp
    (defun my/tar-sort (info)
      (sort info (lambda (a b) (string< (tar-header-name a) (tar-header-name b)))))
    ;; Apply to tar-parse-info in tar-summarize-buffer.
    ```

- add a command or minor mode to highlight marks; serving suggestion:

    ``` elisp
    (save-excursion
      (dolist (m mark-ring)
        (goto-char (marker-position m))
        (pulse-momentary-highlight-region (point) (point-at-eol))
        (sit-for 0.1)))
    ```

- StackOverflow-like suggestions when reporting bugs
- make vc honour `.gitignore` so that `project-find-file` completion
  is not cluttered with ignored files
- in a folder with a .git folder and a .gitignore file,
    - `C-x C-f .gi TAB` completes to .gitignore
    - `C-x C-f .git TAB` says "not unique"
- add some Custom machinery to save additions/removals to list
  variables; as things stand, once I add/remove items and save the
  variable, I miss out on changes to the variable's default value:
  e.g. `git-commit-setup-hook` recently added `bug-reference-mode`,
  but I never realized it because I customized the hook to add some
  personal functions
- some bindings advertised by `describe-mode` in texinfo-mode are
  invalid:
    - `C-c C-s` runs `Texinfo-insert-node` instead of
      `texinfo-show-structure`
    - `C-c C-e` runs `Texinfo-environment` instead of being a prefix
      command for `texinfo-format-{buffer,region}`
- when truncating long lines and auto-filling, if the window is
  narrower than (- fill-column hscroll-margin), horizontal scrolling
  kicks in before the line is auto-filled; the window (or the current
  line, depending on auto-hscroll-mode) remains horizontally stuck to
  the right even though auto-filling made the line short enough to
  display without hscroll

[Unicode conventions]: https://lists.gnu.org/archive/html/emacs-devel/2018-07/msg00961.html

## Dired

- allow "columns" to be toggled; more specifically, make it possible
  to hide ownership, permissions, number of hard links…

- make the filename stand out

- `% r` does not support some `\` constructs (`\#`, `\,`)

- add a face for broken symbolic links

## ERC

- move timestamp to a better position (see `stamp` module)
- play well with whitespace-mode

## Org

- dissociate sub- and super-script fontification from everything else
  in org-toggle-pretty-entities

- make org-ctrl-c-ctrl-c or org-clock-in call
  `(org-clock-update-mode-line t)` when point is on the clocked-in
  heading

- new command `org-clock-in-late` to clock out x minutes ago from
  previous task, and clock in x ago on task at point

- the org-hide face is computed when org-mode is activated;
  customizing the default background makes it obsolete until the mode
  is reverted

## read-passwd

- prevent kills from ending up in the clipboard
- add command to temporarily reveal password

## whitespace-mode

- skip line/wrap-prefix variables and properties

- highlights the adaptive-wrap prefix, although weirdly enough the
  whitespace *faces* are not applied

- in org-indent-mode, adds a spurious (unfaced) space marker before
  paragraphs; somehow transient-mark-mode knows not to highlight this
  space, maybe the answer lies there

## Ellipses customization

Make ellipses for "invisible" text easily customizable (src/xdisp.c).
Make them default to "…" when `(char-displayable-p ?…)`.

Use-cases:

- ibuffer truncation
- hideshow
- outline (org)
- rgrep
- magit-process

## xref

- search gz-compressed Elisp files when calling `xref-find-references`
  from an Elisp buffer: from a user perspective, if
  `xref-find-definitions` can visit those files,
  `xref-find-references` should work too (even though technically they
  don't rely on the same mechanisms)

- defun-building macros seem hard to understand for xref: e.g. `M-.`
  on `rg` does not jump to `rg-define-search rg`

## shr

- eww, gnus-article: adapt filling to window width changes
    - alternately, teach shr not to fill lines; rely on
      visual-line-mode (cf. [wasamasa's monkey-patch in nov.el])

[wasamasa's monkey-patch in nov.el]: https://github.com/wasamasa/nov.el/commit/ecbdecc927a3b3f7e0927d225e6e6464c244c2ae

## External libraries

### debbugs

- asynchronous bug fetching

### magit

- customize current-tag function so that one can add `--first-parent`
- prevent section highlight overlay from hiding tag face background
- when quitting ediff conflict resolution, the "save buffer" prompt is
  out of focus, one must ALT-TAB out of the ediff control panel first
  (noticed in Emacs 27; 25.1 works fine); possible culprits:
    - magit
    - ediff
    - smerge
    - yes-or-no-p
    - read-answer
- make status buffer understand and display `typechange` e.g. when
  replacing a replacing a regular file with a symlink
- when ignoring a file in a subdirectory, magit attempts to
  `git add .gitignore` (i.e. toplevel `.gitignore`), which fails
- for repos with dozens of old branches, it would be nice to have a
  way to hide or distinguish "stale" branches from active ones, a la
  <https://emba.gnu.org/emacs/emacs/branches>

#### transient

- cannot use the full domain of `--find-renames` (e.g. 1%)

### markdown-mode

- support [shortcut reference links]
- move point past header after C-c C-t !/@
- update sub-superscript regex to allow L~i+1~
- make justification work with indented blockquotes
- add "ini ↦ conf-mode" to markdown-code-lang-modes

[shortcut reference links]: http://spec.commonmark.org/0.27/#shortcut-reference-link

### page-break-lines

- borked in magit-diff

### diff-hl

- colored fringe line is discontinued on lines featuring faces with
  heights greater than 1

  ``` shell
  $ git init
  $ echo "* foo" > README.org
  $ git add README.org ; git commit -m README.org README.org
  $ echo "* bar" >> README.org
  $ echo "* baz" >> README.org
  ```

  ``` elisp
  (load-file "…/diff-hl.el")
  (global-diff-hl-mode)
  (set-face-attribute 'org-level-1 nil :height 1.2)
  (find-file "README.org")
  ```

- hides left line-truncation marker

### wgrep

`C-c C-d` (`wgrep-mark-deletion`) could move to the next line, just
like `d` moves to the next line in Dired buffers.
