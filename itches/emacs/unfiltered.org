* eglot + pyls: how to add myproject/src to path so that definition/completion works?
* rg/deadgrep: add user-customizable predicate to control default
--no-ignore (eg nil by default = don't add the flag; or user-function
to add it whenever project-root is ~)
* add "fr10x" to quail-keyboard-layout-alist
then try to figure out how to input dot and comma with russian-computer input method (with describe-input-method)


