# Better out-of-the-box display for FORM FEED

By default, FORM FEED is displayed as a dumb `^L` glyph.  This is
surprising considering it shows up in so many places:

- Emacs source files (C and Elisp),
- help buffers:
    - `describe-mode`,
    - `describe-bindings`,
    - `view-emacs-news`,
- log buffers:
    - `*Compile-Log*`,
    - `*Dired log*`,
- …

You can even see it in source files of other GNU projects, like GCC.

"Pages" are important enough to have their own navigation and
narrowing commands, yet their default delimiter is displayed as an
unassuming control character.

Having this kind of display by default would make it more obvious that
this character has actual navigation semantics; as it stands, it looks
no different from some stray CARRIAGE RETURN (to the point where
people unfamiliar with them [sometimes assume][junk] they are junk).

I like the way form feeds are displayed with [`page-break-lines`];
magit's `show-lines` blaming style achieves a similar look.

This hypothetical thin-line display could be re-used by e.g.:

- `apropos-documentation`, where symbol matches are delimited by
  strings of hyphens,

- `describe-symbol`, where symbol definitions are delimited by text
  properties:

  ``` lisp
  (insert "\n\n"
          (eval-when-compile
            (propertize "\n" 'face '(:height 0.1 :inverse-video t)))
          "\n")
  ```

- Custom buffers, where sections are delimited with a
  999-character-wide underlined space.

(See [emacs-devel][emacs-devel-thin-line] for a list of use-cases for
thin lines, form-feed-related or not.)

Full disclosure
:   This reflection started because moving over this underlined space
    with `truncate-lines` on caused the screen to jump horizontally.
    This specific problem was fixed without dragging FORM FEED display
    into the discussion, but I feel like the latter is the more
    interesting issue ([who on Earth] enables `truncate-lines` by
    default anyway).

[junk]: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=41250#89
[`page-break-lines`]: https://github.com/purcell/page-break-lines
[emacs-devel-thin-line]: https://lists.gnu.org/archive/html/emacs-devel/2019-05/msg00268.html
[who on Earth]: https://gitlab.com/peniblec/dotfiles/blob/master/.emacs-custom.el

## Scratch pad

In a temporary buffer:

```
foobar
^L
foobaz
(put-text-property 8 10 'display (propertize "\n" 'face '(:height 0.1 :inverse-video t)))
```

Possibly needs `:extend t` now?

Grabbing the newline as well as the form-feed could be a way to ensure
we do not fontify `^L`s embedded in strings?
