# XFCE

- read `~/.config/environment.d` to do away with `~/.xsessionrc`
  (e.g. to set `$PATH`)

## xfwm: hide titlebars and decorations

Openbox has an option for that, and I find it neat.  Unfortunately,
the XFCE developers have historically been strongly opposed to this
feature.  Cf. [their old FAQ][xfce-FAQ:wmhints]:

> **6. Why are there no options to start an application without any
> window decoration, to "keep it on top" of other windows, or to make
> it disappear from the taskbar ?**
>
> xfwm4 does not, and will never, include any mechanism to bypass what
> the application wants. The application should allow this by using
> standard hints for Window Managers. Moreover, by doing it in the
> application, it will work with all WM. Gkrellm2 is a good example of
> such implementation.

AFAIK this stance has not changed despite multiple requests on the
mailing list ([xfce:20050822124853.GA5200@areti.co.uk]) and on the bug
tracker ([xfce:8379], [xfce:9082], [xfce:11427], all closed as
WONTFIX).

[xfce-FAQ:wmhints]: https://web.archive.org/web/20040414233213/http://www.xfce.org/index.php?page=documentation&lang=en#wmhints
[xfce:20050822124853.GA5200@areti.co.uk]: https://mail.xfce.org/pipermail/xfce/2005-August/015039.html
[xfce:8379]: https://bugzilla.xfce.org/show_bug.cgi?id=8379
[xfce:9082]: https://bugzilla.xfce.org/show_bug.cgi?id=9082
[xfce:11427]: https://bugzilla.xfce.org/show_bug.cgi?id=11427

As of xfwm4 4.12.5 this can be worked around by creating an empty
custom theme:

``` shell
mkdir -p .local/share/themes/nofrills/xfwm4
touch .local/share/themes/nofrills/xfwm4/themerc
xfconf-query -c xfwm4 -p /general/theme -s nofrills
```

# GTK

## File picker dialog

When opening a file in a GTK program, starting to type a filename
triggers a search instead of jumping to a matching file.  This is
disorienting: instead of moving through the directory I see,
everything suddenly vanishes, then a bunch of unrelated files start
popping up one after another.

It seems GNOME developers are not fond of the "type to jump" UX
(cf. e.g. [rh:902061], [gtk:2366], [nautilus:1157]).

[rh:902061]: https://bugzilla.redhat.com/show_bug.cgi?id=902061
[gtk:2366]: https://gitlab.gnome.org/GNOME/gtk/-/issues/2366
[nautilus:1157]: https://gitlab.gnome.org/GNOME/nautilus/-/issues/1157

# Spell checkers

- update dictionaries?

# Conky

Cannot use `${eval $${somefunc ${gw_iface}}}` more than once:

    conky.text = [[
    ${eval ${gw_iface}}
    ${eval ${gw_iface}}
    ]]
    ⇒
    wlp20
    (null)

See [GitHub issue][conky-issue-461].  Partially fixed by this [pull
request][conky-pull-571], which allows using both `*speedf` functions
together; `*graph` functions still trigger the bug though, e.g. with:

    ${goto 20}${upspeedf ${gw_iface}}KiB/s${goto 90}↑${goto 100}${upspeedgraph ${gw_iface} 12,0 678b8b ffffff -t}
    ${goto 20}${downspeedf ${gw_iface}}KiB/s${goto 90}↓${goto 100}${downspeedgraph ${gw_iface} 12,0 678b8b ffffff -t}

[conky-issue-461]: https://github.com/brndnmtthws/conky/issues/461
[conky-pull-571]: https://github.com/brndnmtthws/conky/pull/571

# Bunsenlabs

- use https for sources.list files
- some manpages for `bl-…` utilities feature a buggy DESCRIPTION
  section, e.g. `bl-kb`:

        DESCRIPTION
           Traceback (most recent call last):
                      File "./bin/bl-kb", line 50, in <module>

                      from lxml import etree

               ImportError: No module named 'lxml'
